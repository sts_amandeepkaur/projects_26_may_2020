import os
import wx
import cv2
import numpy as np
from keras.models import load_model
 
class PhotoCtrl(wx.App):
    def __init__(self, redirect=False, filename=None):
    
        wx.App.__init__(self, redirect, filename)
        self.frame = wx.Frame(None, title='Cat-Dog classification')
 
        self.panel = wx.Panel(self.frame)
 
        self.PhotoMaxSize = 240
 
        self.createWidgets()
        self.frame.SetSize(wx.Size(280,450))
        self.new_model = load_model('cat_dog_fine_tune_moded.h5')
        self.frame.Show()
 
    def createWidgets(self):
        instructions = 'Browse for an image'
        img = wx.EmptyImage(240,240)
        self.imageCtrl = wx.StaticBitmap(self.panel, wx.ID_ANY, 
                                         wx.BitmapFromImage(img))
 
        instructLbl = wx.StaticText(self.panel, label=instructions)
       # self.photoTxt = wx.TextCtrl(self.panel, size=(200,-1))
        browseBtn = wx.Button(self.panel, label='Browse')
        browseBtn.Bind(wx.EVT_BUTTON, self.onBrowse)
 
        predictBtn = wx.Button(self.panel, label='Predict')
        predictBtn.Bind(wx.EVT_BUTTON, self.onpredict)
        
        self.text=wx.StaticText(self.panel,wx.ID_ANY,'Prediction is :')
        font = wx.Font(18, wx.DECORATIVE,wx.NORMAL,wx.BOLD)
        self.text.SetFont(font)
        
        
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
 
        self.mainSizer.Add(wx.StaticLine(self.panel, wx.ID_ANY),
                           0, wx.ALL|wx.EXPAND, 5)
        self.mainSizer.Add(instructLbl, 0, wx.ALL, 5)
        self.mainSizer.Add(self.imageCtrl, 0, wx.ALL, 5)
        #self.sizer.Add(self.photoTxt, 0, wx.ALL, 5)
        self.sizer.Add(browseBtn, 0, wx.ALL, 5) 
        self.sizer.Add(predictBtn, 0, wx.ALL, 5) 
        self.mainSizer.Add(self.text, 0, wx.ALL, 15) 
        
        self.mainSizer.Add(self.sizer, 0, wx.ALL, 5)
 
        self.panel.SetSizer(self.mainSizer)
        
        self.mainSizer.Fit(self.frame)
 
        self.panel.Layout()
 
    def onBrowse(self, event):
        """ 
        Browse for file
        """
        wildcard = "JPEG files (*.jpg)|*.jpg"
        dialog = wx.FileDialog(None, "Choose a file",
                               wildcard=wildcard,
                               )
        if dialog.ShowModal() == wx.ID_OK:
            #self.photoTxt.SetValue(dialog.GetPath())
            self.filepath = dialog.GetPath()
        
        dialog.Destroy() 
        self.onView()
 

    def onpredict(self,event):
        classess = {0:'cat',1:'dog'}
        test=cv2.imread(self.filepath)
        test = cv2.resize(test,(224,224))
        test = test.astype('float')/255
        test = test.reshape(1,224,224,3)
        p=self.new_model.predict(test).argmax()
        print (classess[p])
        label = 'Prediction is : {}'.format(classess[p])
        self.text.SetLabel(label) 
        #text=wx.StaticText(panel,wx.ID_ANY,classess[p],(10,60))
        
        
    def onView(self):
        #filepath = self.photoTxt.GetValue()
        img = wx.Image(self.filepath, wx.BITMAP_TYPE_ANY)
        # scale the image, preserving the aspect ratio
        W = img.GetWidth()
        H = img.GetHeight()
        if W > H:
            NewW = self.PhotoMaxSize
            NewH = self.PhotoMaxSize * H / W
        else:
            NewH = self.PhotoMaxSize
            NewW = self.PhotoMaxSize * W / H
        img = img.Scale(NewW,NewH)
 
        self.imageCtrl.SetBitmap(wx.BitmapFromImage(img))
        self.panel.Refresh()
 
if __name__ == '__main__':
    
    app = PhotoCtrl()
    app.MainLoop()
